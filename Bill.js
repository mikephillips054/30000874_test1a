/*Name: Mike Phillips 
Student ID: 30000874 
Date: 29/03/2018
Bill*/

let user_id = 0;
let username = "";
let user_unit = 0;
let user_charge = 0.
var n = user_charge.toFixed(2)
let user_cost = 0;
var n = user_cost.toFixed(2)
let output = ""

user_id= Number(prompt("Enter your customer ID number:"));
username = prompt("Please enter your name");
user_unit = Number(prompt(`Please enter the amount of units ${username} has used`))
output = `
Calculating the customer's Electricity Bill:
----------------------------------------------
 ID:                 ${user_id}
 Name:               ${username}
 Units:              ${user_unit}
 
 Total Amount Due:
 ---------------------------------------------
`

if( user_unit < 200)
{
    user_charge = 1.20;
}
else if( user_unit >= 200 && user_unit < 400)
{   
    user_charge = 1.50
}
else if( user_unit >= 400 && user_unit < 600)
{   
    user_charge = 180
}
else if (user_unit >= 600 )
{
    user_charge = 2.00

}

user_cost = user_charge * user_unit

console.log(`${output} 
 Total Owing @ ${user_charge} is: $${user_cost} `)