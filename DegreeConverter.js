/*Name: Mike Phillips 
Student ID: 30000874 
Date: 29/03/2018
Converting Celsius to Fahrenheit*/

//Stating Variables
let celsius = 0 
let fahrenheit = 0
let output = `Temperature Conversion
----------------------------
The temperature in Fahrenheit is:
 ---------------------------------------------`

//Calculation
celsius = Number(prompt("Please enter the temperture in Celsius")) ;
fahrenheit = celsius * 18 / 10 + 32;

//Output
console.log(`${output}
${fahrenheit} degrees`)